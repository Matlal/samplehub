# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Users
User.all.delete_all

if User.all.empty? 
	user  = User.create!(name:  "Mads",
				 email: "testingAdmin@SampleHub.com",
				 username: "ADMIN",
					password: "MADS22",
					password_confirmation: "MADS22",
					admin: true,
					activated: true,
					activated_at: Time.zone.now.to_datetime )

# WARNING: if development mails are allowed don't create as many users as done below:

# following is data for SHOW 
# Todos

3.times do |k|
	
	10.times do |n|
		
		title = Faker::Name.name #here title is the name of the todo
		description = Faker::Lorem.sentence(5)
		label=Faker::Name.title
		prio = Faker::Number.between(1, 10) 
		status = k+1
		user.todos.create!(title:title,
			description: description,
			status:status,
			prio:prio,
			label:label
			)
	end
end
user.todos.build()
# Users
	99.times do |n|
		name  = Faker::Name.name
		email = "example-#{n+1}@railstutorial.org"
		username = "example-#{n+1}"
		password = "password"
		User.create!(name:  name,
			email: email,
			username: username,
			password:              password,
			password_confirmation: password,
			activated: true,
			activated_at: Time.zone.now
		)
	end

	#Microposts
	users = User.order(:created_at).take(6)
	50.times do
	  content = Faker::Lorem.sentence(5)
	  users.each { |u| u.microposts.create!(content: content) }
	end

	# Following relationships
	users = User.all
	
	following = users[2..50]
	followers = users[3..40]
	following.each { |followed| user.follow(followed) }
	followers.each { |follower| follower.follow(user) }

end

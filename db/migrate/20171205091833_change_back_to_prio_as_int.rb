class ChangeBackToPrioAsInt < ActiveRecord::Migration[5.1]
    def up

        add_column :todos, :prio, :integer, :default => 2
        Todo.find_each do |todo|
            case todo.priority
            when "Low"
                todo.prio=1
            when "Medium"
                todo.prio=2
            when "High"
                todo.prio=3
            when "Critical"
                todo.prio=4
            end
            todo.save!
        end
        remove_column :todos, :priority
        end

=begin
    def down
        add_column :todos, :priority, :string, :default => "Medium"
        Todo.find_each do |todo|
            case todo.prio
            when 1
                todo.priority="Low"

            when 2
                todo.priority="Medium"

            when 3
                todo.priority="High"

            when 4
                todo.priority="Critical"

            else
                todo.priority="Medium"
            end
            todo.save!
        end
         remove_column :todos, :prio
      end
=end
end

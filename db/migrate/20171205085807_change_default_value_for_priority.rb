class ChangeDefaultValueForPriority < ActiveRecord::Migration[5.1]
  def up
      change_column :todos, :priority, :string, :default => "Low"
  end
end

class CreateTodos < ActiveRecord::Migration[5.0]
  def change
    create_table :todos do |t|
      
      t.string :title,  null: false
      t.text :description
      t.integer :status, default: 3 
      t.integer :prio, default: 5
      t.string :label

      t.timestamps
	  
	  t.references :user, foreign_key: true
    end
  end
end

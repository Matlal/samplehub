class CreateUsers < ActiveRecord::Migration[5.0]
	def change
		create_table :users do |t|
			t.string :name
			t.string :email, null: false
			#Add username
			# t.string :username, null: false
			#Add password_digest
			t.string :password_digest
			#Add Admin
			t.boolean :admin, default: false
			#Add remember_digest
			t.string :remember_digest
			#Add activation
			t.string :activation_digest
			t.boolean :activated, default: false
			t.datetime :activated_at
			#Add reset
			t.string :reset_digest
			t.datetime :reset_sent_at
			
			t.timestamps
		end
		#Add index
		add_index :users, :email, unique: true
		# add_index :users, :username, unique: true
	end
end

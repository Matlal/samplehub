class ChangePrioToBeStringInTodos < ActiveRecord::Migration[5.1]
  def change
    add_column :todos, :priority, :string
    Todo.find_each do |todo|

        if(todo.prio<4)
            todo.priority="Low"
        else
            if(todo.prio<7)
                todo.priority="Medium"
            else
                if(todo.prio<9)
                    todo.priority="High"
                else
                    todo.priority="Critical"
                end
            end
        end
        todo.save!
    end
    remove_column :todos, :prio
  end

 # def down
#    remove_column :todos, :priority
#    add_column :todos, :prio, :integer, default: 5
#  end
end

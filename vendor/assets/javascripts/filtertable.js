function filtertable(n, shortcut) {
	// Declare variables
	var input, filter, table, tr, td, i;
	var el = document.getElementById("filterfield1");
	if(n==-1){
		n=4;
		if(shortcut!=""){
			el.style.display = "none";
		}
		else{
			el.style.display = "block";
			document.getElementById('myInput').value ="";
		}

		filter= shortcut;
	}
	else{

		el.style.display = "block";
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
	}
	// console.log(n);
	// console.log(shortcut);

	table = document.getElementById("myTable");
	tr = table.getElementsByTagName("tr");

	//Loop through all table rows, and hide those who don't match the search query
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[n];
		if (td) {
			if (td.innerHTML.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
				tr[i].style.display = "";
				} else {
				tr[i].style.display = "none";
			}
		}
	}
}

module TodosHelper

    def getInvertedStatusArr
        {"Done" => 1 ,"In Progress" => 2, "Not Started"=>3}
    end
    def getInvertedPrioArr
        {"Low"=>1,"Medium"=>2,"High"=>3,"Critical"=>4}
    end
    def getStatusArr
        ["Done", "In Progress", "Not Started"]
    end
    def getPrioArr
        ["Low", "Medium", "High", "Critical"]
    end
    def convertObjIntToString(var,obj)
        case var
            when "Status"
            getStatusArr[obj-1]
            when "Prio"
            getPrioArr[obj-1]
        end
    end
end

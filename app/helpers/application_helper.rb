module ApplicationHelper


  def app_name
     Rails.application.class.parent_name
  end
# Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title =  app_name
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def get_admin_email
    User.find_by(:admin=>true).email
  end
  def get_max_char_amount
      Micropost.validators_on( :content ).first.options[:maximum]
  end
end

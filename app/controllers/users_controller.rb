class UsersController < ApplicationController
	before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
	before_action :correct_user,   only: [:edit, :update]
	before_action :admin_or_correct_user,     only: :destroy


	def index
		numberofpages = User.paginate(page: 1).total_pages
		if(params[:page].to_i > numberofpages)
			flash[:info] = "This is the last existing users page"

			redirect_to users_path(page: numberofpages)
		else
			@users = User.paginate(page: params[:page])
		end
		store_location
	end
	def show
		@user = find(params[:id])
		@microposts = @user.microposts.paginate(page: params[:page])
		if logged_in?
			@micropost  = @user.microposts.build
			todoList = @user.todos

			@todos1 = todoList.where("status = 1")

			@lastesttodos1 = @todos1.limit(5).order("updated_at DESC")

			@todos2 = todoList.where("status = 2")
			@lastesttodos2 = @todos2.limit(5).order("updated_at DESC")

			@todos3 = todoList.where("status = 3")
			@lastesttodos3 = @todos3.limit(5).order("created_at DESC")

			@newtodo  = @user.todos.build

			# @feed_items = current_user.feed.paginate(page: params[:page])
		end
	end
	def new
		@user = User.new
	end
	def create
		@user = User.new(user_params)
		if @user.save

			@user.send_activation_email
			@admin = User.find_by(:admin=>true)
			@admin.send_AdminNotification_email(@user.id)
			flash[:info] = "Please check your email to activate your account."
			redirect_to root_url
		else
			render 'new'
		end
	end
	def find(id)
		User.find(id)
	end
	def edit

	end
	def update
		if @user.update_attributes(user_params)
			flash[:success] = "Profile updated"
			redirect_to @user
		else
			render 'edit'
		end
	end
	def destroy
		find(params[:id]).destroy
		flash[:success] = "User deleted"
		redirect_back_or(users_url)
	end
	def following
		@title = "Following"
		@user  = find(params[:id])
		@users = @user.following.paginate(page: params[:page])
		render 'show_follow'
	end

	def followers
		@title = "Followers"
		@user  = find(params[:id])
		@users = @user.followers.paginate(page: params[:page])
		render 'show_follow'
	end

	private

	def user_params
		params.require(:user).permit(:name, :username, :email, :password, :password_confirmation)
	end

	# Before filters

	# Confirms the correct user.
	def correct_user
		@user = find(params[:id])
		redirect_to(root_url) unless current_user?(@user)
	end
	# Confirms an admin user.
	def admin_or_correct_user
		if !current_user.admin?
			@user = find(params[:id])
			if !current_user?(@user)
				redirect_to(root_url) unless current_user.admin?
			end
		end
	end

end

class TodosController < ApplicationController
	before_action :logged_in_user
	before_action :current_todo,   only: [:edit, :destroy, :update]
	
	def index
		@activetodos = current_user.todos.where(status: [2,3])
		@filter=session[:f]
		@todos = current_user.todos

		@newtodo = Todo.new

	end
	def edit
	end


	def storefilter
		session[:f] = params[:f]
		@filter=params[:f]
		redirect_to todos_path
	end
	# def show
		# respond_to do |format|
			# format.json do
				# # Return kid as json object
				# puts "json format"
				# result = @todo.attributes.to_json
				# # If you want to return the whole kid record attributes in json: result = kid.attributes.to_json
				# render json: result
			# end
			# format.js do
				# puts "js format"
			# end
		# end
	# end
	def create
		@todo = current_user.todos.build(todo_params)
		if @todo.save
			flash[:success] = "Task created!"
			redirect_to request.referrer
			else
			string = "Error messages: "
			@todo.errors.full_messages.each do |msg|
				string=string+": "+msg
			end
			flash[:danger] =  string
			redirect_to request.referrer
		end
	end

	def update
		respond_to do |format|
			if @todo.update_attributes(todo_params)
                flash[:success] = "Task updated!"
				format.json { head :no_content }
				format.js
                redirect_to request.referrer
				else
				format.json { render json: @todo.errors.full_messages,
				status: :unprocessable_entity }
			end

		end

	end
	def destroy
		@todo.destroy
        flash[:danger] = "Task deleted!"
		respond_to do |format|
			format.js
			format.html { redirect_to todos_url }
			format.json { head :no_content }
		end
		redirect_to request.referrer
	end

	private

    def todo_params
		params.require(:todo).permit( :title, :description,:status,:prio,:label )
	end
	def current_todo
		@todo = current_user.todos.find_by(id: params[:id])
		redirect_to root_url if @todo.nil?
	end

end

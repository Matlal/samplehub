class Todo < ApplicationRecord
	belongs_to :user
	
#	default_scope -> { order(prio: :desc) }

#   TODO.prio = {1:"Low",2:"Medium",3:"High",4:"Critical" }
    # ["Low", "Medium", "High", "Critical"]
    # [1, 2, 3, 4]

	validates :user_id, presence: true
	validates :title, presence: true, length: { maximum: 50 }
	VALID_PRIOS = [1, 2, 3, 4]
    validates :prio, inclusion: { in: VALID_PRIOS, message: "%{value} is not a valid Priority value, contact the administrator if this error accour again!" }, allow_blank: true
	def local_time(time)
		if time == "updated" and !self.updated_at.nil?
			self.updated_at.localtime.strftime "%Y-%m-%d %H:%M:%S"
		elsif time == "created" and !self.created_at.nil?
			self.created_at.localtime.strftime "%Y-%m-%d %H:%M:%S"
		end
	end

end

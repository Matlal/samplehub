class UserMailer < ApplicationMailer
	
	# Subject can be set in your I18n file at config/locales/en.yml
	# with the following lookup:
	#
	#   en.user_mailer.account_activation.subject
	#
	def account_activation(user)
		@user = user
		mail to: user.email, subject: "Account activation"
	end


	# Subject can be set in your I18n file at config/locales/en.yml
	# with the following lookup:
	#
	#   en.user_mailer.password_reset.subject
	#
	def password_reset(user)
		@user = user
		mail to: user.email, subject: "Password reset"
	end

	def new_created_user_notification(admin, newuserid)

		@admin = admin
		@newUser = User.find_by(:id => newuserid)
		# puts @user.id
		# puts @admin.id
		# puts @user
		mail to: @admin.email, subject: "New user was created"

	end
end

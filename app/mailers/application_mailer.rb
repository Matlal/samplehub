class ApplicationMailer < ActionMailer::Base
	default from: "noreply@example.com"
	layout 'mailer'
	add_template_helper(ApplicationHelper)
	def sample_email(user)
		@user = user
		mail(to: @user.email, subject: 'Sample Email')
	end

end

require 'test_helper'

class TodoTest < ActiveSupport::TestCase
	def setup
		@user = users(:michael)
		# This code is not idiomatically correct.
		@todo = @user.todos.build(title: "Lorem ipsum")
	end
	
	test "should be valid" do
		assert @todo.valid?
	end
	
	test "user id should be present" do
		@todo.user_id = nil
		assert_not @todo.valid?
	end
	
	test "title should be present" do
		@todo.title = "   "
		assert_not @todo.valid?
	end
	
	test "content should be at most 140 characters" do
		@todo.title = "a" * 51
		assert_not @todo.valid?
	end
	
end

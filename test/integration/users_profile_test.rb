require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
    include ApplicationHelper

	def setup
		@user = users(:michael)
		@other = users(:archer)
	end

	test "profile display of NOT logged in user(shortcuts shall NOT appear)" do
		get user_path(@user)
		assert_not is_logged_in?
		assert_template 'users/show'
		assert_select 'title', full_title(@user.name)
		assert_select 'h3', text: @user.name
		assert_select 'img'
		assert_match @user.microposts.count.to_s, response.body
		assert_select "a[href=?]", edit_user_path(@user), count: 0 #No settings
		assert_select 'div.pagination'
		@user.microposts.paginate(page: 1).each do |micropost|
			assert_match micropost.content, response.body
		end
	end
	test "profile display of logged in user(shortcuts shall
			appear properly)" do
		get login_path
		post login_path, params: { session: { login:    @user.username,
									password: 'password' } }
		get user_path(@user)
		assert is_logged_in?
		assert_template 'users/show'
		assert_select 'title', full_title(@user.name)
		assert_select 'h3', text: @user.name
		assert_select 'img'
		assert_match @user.microposts.count.to_s, response.body
		assert_select "a[href=?]", edit_user_path(@user)
		assert_select 'div.pagination'
		@user.microposts.paginate(page: 1).each do |micropost|
			assert_match micropost.content, response.body
		end
		get user_path(@other)
		assert_select "a[href=?]", edit_user_path(@other), count: 0
	end
end

require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  def setup
		@appname = app_name
	end
  test "full title helper" do
    assert_equal full_title, app_name
    assert_equal full_title("About"), "About | #{@appname}"
  end
end

# Sample Hub

This is my version of the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).

## Removed functionality of the tutorial application:
- [(13.4.4) Image upload in production](https://www.railstutorial.org/book/user_microposts#sec-image_upload_in_production) 
## Added features:
* TaskManager/Todo list for every user

Some small features:

 * Users can delete themselfs
 * word count for micropost
 

:)